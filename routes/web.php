<?php

// Rota para redirecionar
Route::redirect('/', '/login');

// Rotas de autenticação
Route::get('/login', 'Auth\LoginController@index')->name('login.index');
Route::post('/login', 'Auth\LoginController@authenticate')->name('login.authenticate');

// Rotas de esqueci minha senha
Route::get('/recuperar', 'Auth\ForgotPasswordController@index')->name('recover.index');
Route::post('/recuperar', 'Auth\ForgotPasswordController@register')->name('recover.create');

// Rota de logout
Route::get('/logout', 'Auth\LoginController@logout')->name('login.logout'); 

// Rotas do painel
Route::group(['prefix' => 'painel', 'middleware' => 'auth'], function () {
    
    Route::get('/pipedrive', 'NavigationController@pipedrive')->name('nav.pipedrive');
    
    // Página inicial
    Route::get('/', 'NavigationController@index')->name('nav.index');    
    Route::get('/preparacao', 'NavigationController@preparacao')->name('nav.preparacao');
    Route::get('/negociacao', 'NavigationController@negociacao')->name('nav.negociacao');
    
    // Rota de registro de usuário
    Route::post('/cadastro', [
        'uses' => 'NavigationController@register',
        'as' => 'register.create',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // Rotas de gerenciamentos de usuários
    Route::get('/cadastro', [
        'uses' => 'NavigationController@cadastro',
        'as' => 'nav.cadastro',
        'middleware' => 'roles',
        'roles' => ['Admin']  
    ]);

    Route::get('/usuarios', [
        'uses' => 'NavigationController@usuarios',
        'as' => 'nav.usuarios',
        'middleware' => 'roles',
        'roles' => ['Admin']  
    ]);

    Route::get('/usuarios/{id}', [
        'uses' => 'NavigationController@usuario',
        'as' => 'nav.usuario',
        'middleware' => 'roles',
        'roles' => ['Admin']  
    ]);

    Route::post('/usuarios/deletar/{id}', [
        'uses' => 'NavigationController@deleteuser',
        'as' => 'nav.deleteuser',
        'middleware' => 'roles',
        'roles' => ['Admin']  
    ]);
    
});


// Rota de fallback
Route::fallback(function(){
    return view('404');
});