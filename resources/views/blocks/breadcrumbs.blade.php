<div class="breadcrumb-bar navbar bg-white sticky-top">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">

            @foreach ($breadcrumbs as $breadcrumb)
                {!! $breadcrumb['icon'] !!}
                <li class="breadcrumb-item {{ $loop->last ? 'active' : '' }}">
                    @if ($loop->last)
                        {{ $breadcrumb['title'] }}
                    @else
                        <a href="{{ $breadcrumb['route']  }}">
                            {{ $breadcrumb['title'] }}
                        </a>
                    @endif
                </li>
            @endforeach 

        </ol>
    </nav>

    @if (Auth::user()->roles->first()->name === 'admin')
        
    <div class="dropdown">
        <button class="btn btn-round" role="button" data-toggle="dropdown" aria-expanded="false">
            <i class="material-icons">settings</i>
        </button>
        <div class="dropdown-menu dropdown-menu-right">

            <a class="dropdown-item" href="{{ route('nav.cadastro') }}">Criar Novo Usuário</a>
            <a class="dropdown-item" href="{{ route('nav.usuarios') }}">Gerenciar Usuários</a>
            {{-- <a class="dropdown-item" href="nav-top-kanban-board.html#">Mark as Complete</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item text-danger" href="nav-top-kanban-board.html#">Archive</a> --}}

        </div>
    </div>

    @endif

</div>
