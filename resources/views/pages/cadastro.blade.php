@extends('layout.default')
@section('contents')
@include('blocks.breadcrumbs')


<div class="main-container">

    <section class="create-form mt-3">

        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">

                    <form method="POST" action="{{ route('register.create') }}">
                        @csrf
                        <div class="form-row mt-2">
                            <div class="col-8">
                                <div class="input-group input-group-round">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">email</i>
                                        </span>
                                    </div>
                                    <input type="text" name="email" class="form-control"
                                        placeholder="Endereço de E-mail" aria-label="Email Address">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="input-group input-group-round">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">vpn_key</i>
                                        </span>
                                    </div>
                                    <input type="text" name="password" class="form-control" placeholder="Senha"
                                        aria-label="Senha">
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="col-8">
                                <div class="input-group input-group-round">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">person_pin</i>
                                        </span>
                                    </div>
                                    <input type="text" name="name" class="form-control" placeholder="Nome Completo"
                                        aria-label="Nome Completo">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="checkbox-wrapper">
                                    <input name="admin" type="checkbox" class="form-check-input" id="admin">
                                    <label class="form-check-label ml-2" for="admin"><b>Administrador</b></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="pipelines"><b>Selecione os Pipelines desejados:</b></label>
                                    <select name="pipelines[]" multiple class="form-control" id="pipelines">
                                        @foreach ($pipelines as $pipeline)
                                        <option value="{{ $pipeline->id }}">{{ $pipeline->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-3 d-flex justify-content-end">
                            <input type="submit" class="btn btn-primary" value="Cadastrar"></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

    </section>



</div>
</div>

@endsection
