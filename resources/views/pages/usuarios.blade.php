@extends('layout.default')
@section('contents')
@include('blocks.breadcrumbs')

<div class="main-container">
    <section class="user-table">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Usuários cadastrados</h5>

                            <table id="users-table" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nome</th>
                                        <th scope="col">E-mail</th>
                                        <th scope="col">Função</th>
                                        <th scope="col">Data de Cadastro</th>
                                        <th scope="col">Opções</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($usuarios as $usuario)
                                    <tr>
                                        <th scope="row">{{ $usuario->id }}</th>
                                        <td>{{ $usuario->name }}</td>
                                        <td>{{ $usuario->email }}</td>
                                        <td>{{ $usuario->roles()->first()->description }}</td>
                                        <td>{{ date('d/m/Y', strtotime($usuario->created_at)) }}</td>
                                        <td>
                                            <a href="#"><i class="material-icons">edit</i></a>
                                            <a href="#"><i class="material-icons">delete</i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="btn btn-primary float-right">Cadastrar novo</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
