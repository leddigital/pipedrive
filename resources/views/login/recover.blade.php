@extends('layout.login')
@section('content')
    
<div class="form-body">
    <div class="website-logo">
        <a href="index.html">
            <div class="logo">
                <img class="logo-size" src="images/logo-light.svg" alt="">
            </div>
        </a>
    </div>
    <div class="row">
        <div class="img-holder">
            <div class="bg"></div>
            <div class="info-holder">
                <img src="images/graphic1.svg" alt="">
            </div>
        </div>
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <h3>Password Reset</h3>
                    <p>To reset your password, enter the email address you use to sign in to iofrm</p>
                    <form>
                        <input class="form-control" type="text" name="username" placeholder="E-mail Address" required>
                        <div class="form-button full-width">
                            <button id="submit" type="submit" class="ibtn btn-forget">Send Reset Link</button>
                        </div>
                    </form>
                </div>
                <div class="form-sent">
                    <div class="tick-holder">
                        <div class="tick-icon"></div>
                    </div>
                    <h3>Password link sent</h3>
                    <p>Please check your inbox <a href="../../../cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="dfb6b0b9adb29fb6b0b9adb2abbab2afb3beabbaf1b6b0">[email&#160;protected]</a></p>
                    <div class="info-holder">
                        <span>Unsure if that email address was correct?</span> <a href="forget4.html#">We can help</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection