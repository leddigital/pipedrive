@extends('layout.login')
@section('content')
    
<div class="form-body">
    <div class="website-logo">
        <div class="logo" style="background-image: url('{{ 'images/logo.png' }}')">
            <img class="logo-size" src="{{ asset('images/logo.png') }}" alt="">
        </div>
    </div>
    <div class="row">
        <div class="img-holder">
            <div class="bg"></div>
            <div class="info-holder">
                <img src="{{ asset('images/graphic1.svg') }}" alt="">
            </div>
        </div>
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <h3>Get more things done with Loggin platform.</h3>
                    <p>Access to the most powerfull tool in the entire design and web industry.</p>
                    <form method="POST" action="{{ route('login.authenticate') }}">
                        @csrf
                        <input class="form-control" type="email" name="email" placeholder="E-mail" required>
                        <input class="form-control" type="password" name="password" placeholder="Senha" required>
                        <div class="form-button">
                            <button id="submit" type="submit" class="ibtn">Login</button> 
                            <a href="{{ route('recover.index') }}">Esqueceu sua senha?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection