// Custom JS

function checkAdminIsChecked () {
    
    var adminSelect = $('#admin');
    var selectPipeline = $('#pipelines');

    if (adminSelect.is(':checked')) 
        disableELement(selectPipeline, true);

    adminSelect.change(function(){
        adminSelect.is(':checked') 
            ? disableELement(selectPipeline, true) 
            : disableELement(selectPipeline, false);
         
    });

}

function disableELement(element, state) {
    element.prop('disabled', state);
    if (state) element.val('');
}

$(document).ready(function(){
    checkAdminIsChecked();
    // initializeDatatable();
});