<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
                
        $role_admin = new Role();    
        $role_admin->name = 'admin';
        $role_admin->description = 'Administrador';
        $role_admin->created_at = date("Y-m-d H:i:s");
        $role_admin->updated_at = date("Y-m-d H:i:s");
        $role_admin->save();
        
        $role_user = new Role();    
        $role_user->name = 'user';
        $role_user->description = 'Usuário';
        $role_user->created_at = date("Y-m-d H:i:s");
        $role_user->updated_at = date("Y-m-d H:i:s");
        $role_user->save();

    }

    
}