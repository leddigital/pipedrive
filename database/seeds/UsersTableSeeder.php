<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        $roleAdmin = Role::where('name', 'admin')->get()->first();
        $roleUser = Role::where('name', 'user')->get()->first();

        // Seeder de Admin
        $admin = new User();    
        $admin->name = 'Waelte Ferraz';
        $admin->email = 'raizbrasil@raizbrasil.global';
        $admin->password = Hash::make('teste123');
        $admin->save();
        $admin->roles()->attach($roleAdmin);
        
        // Seeder de Usuários
        $user = new User();
        $user->name = 'Kinino';
        $user->email = 'kinino@kinino.global';
        $user->password = Hash::make('teste123');
        $user->save();
        $user->roles()->attach($roleUser);


    }

    
}
