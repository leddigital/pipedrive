<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pipeline extends Model
{
    protected $table = 'pipelines';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'name',
        'url_title',
        'add_time',
        'update_time'
    ];

    public $timestamps = false;

    public function deals()
    {
        return $this->hasMany('App\Deals');
    }

    public function user() {
        return $this->belongsToMany('App\User', 'users_has_pipelines');
    }

}
