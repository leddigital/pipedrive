<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Devio\Pipedrive\Pipedrive;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Deal;
use App\Pipeline;
use App\Role;

class NavigationController extends Controller
{

    public $pipedrive;
    public $deal;
    public $pipeline;
    public $user;
    public $role;
    
    public function __construct(){

        $this->pipedrive = new Pipedrive(env('PIPEDRIVE_TOKEN'));
        $this->deal = new Deal();
        $this->pipeline = new Pipeline();
        $this->user = new User();
        $this->role = new Role();
    }

    public function index() {

        $breadcrumbs[] = [
            'icon' => '<i class="material-icons">home</i>',
            'title' => 'Página Incial',
            'route' => route('nav.index'),
         ];

        return view('pages.index', [
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    
    public function pipedrive() {
        $this->pipelineGetData();
        // $this->dealsGetData();
    }
    
    public function preparacao(Request $request) {
        return view('pages.preparacao');
    }

    public function negociacao(Request $request) {
        return view('pages.negociacao');
    }

    public function readusers () {
        $users = $this->user->get()->all();
        
        $data['data'] = $users;
        echo json_encode($data);
    }

    public function usuarios() {


        $breadcrumbs[] = [
            'icon' => '<i class="material-icons">home</i>',
            'title' => 'Página Incial',
            'route' => route('nav.index'),
         ];
 
         $breadcrumbs[] = [
            'icon' => '',
             'title' => 'Gerenciar Usuários',
             'route' => route('nav.usuarios'),
         ];
        
         $usuarios = $this->user->get();

        return view('pages.usuarios', [
            'breadcrumbs' => $breadcrumbs,
            'usuarios' => $usuarios
        ]);

    }


    public function cadastro() {

        $breadcrumbs[] = [
            'icon' => '<i class="material-icons">home</i>',
            'title' => 'Página Incial',
            'route' => route('nav.index'),
        ];

        $breadcrumbs[] = [
            'icon' => '',
            'title' => 'Gerenciar Usuários',
            'route' => route('nav.usuarios'),
        ];

        $breadcrumbs[] = [
            'icon' => '',
            'title' => 'Cadastrar Novo Usuário',
            'route' => route('nav.cadastro'),
        ];

        $pipelines = $this->pipeline->orderBy('name', 'ASC')->get();
        return view('pages.cadastro', [
            'breadcrumbs' => $breadcrumbs,
            'pipelines' => $pipelines
        ]);

    }

    public function organizationsGetData() {

        $organizations = $this->pipedrive->organizations->all(['limit' => 500])->getData();
        
        echo "<pre>";
        print_r($organizations);
        echo "</pre>";

    }

    public function pipelineGetData() {
               
        // Recebe os valores de todos os Pipelines
        $pipelines = $this->getPipelines();

        // Percorre todos os Pipelines
        foreach ($pipelines as $key => $pipeline) {
            
            $entity = array();
            
            // Percorre individualmente cada Pipeline para preencher o array
            foreach ($pipeline as $key => $value) {
                $entity[$key] = $value;
            }

            // Verifica se existe o registro no banco
            $pip = $this->pipeline->find($pipeline->id);

            // Se existir registro update, se não create
            empty($pip) && is_null($pip) 
                ? $this->pipeline->create($entity)
                : $pip->update($entity);
    
        }

    }

    public function dealsGetData() {

        $deals = $this->getPipelines();

        echo "<pre>";
        print_r($deals);
        echo "<pre>";

    }

    public function getPipelines() {
        return $this->pipedrive->pipelines->all()->getData();
    }

    public function register(Request $request) {
       
        $form = $request->only(['name', 'email', 'password', 'pipelines', 'admin']);

        $data = $this->validator($form);
        $form['password'] = Hash::make($form['password']);
            
        $reponse = "";
        
        if($data->fails()){
            $reponse = redirect()->route('register.create');
        } else {

            if($entity = $this->user->create($form)) {
               
                if(isset($form['admin'])) {
        
                    $pipelines = $this->pipeline->get();
                    foreach($pipelines as $pipeline) {
                        $entity->pipeline()->attach($pipeline->id);
                    }

                    $role = $this->role->where('name', 'admin')->get()->first();
                    $entity->roles()->attach($role);

                } else {

                    foreach ($form['pipelines'] as $pipeline) {
                        $entity->pipeline()->attach($pipeline);
                    }

                    $role = $this->role->where('name', 'user')->get()->first();
                    $entity->roles()->attach($role);
                }

                $reponse = redirect()->route('nav.cadastro');

            } else {
                redirect()->route('register.create');
            }
        }
        return $reponse;
    }
    
}
