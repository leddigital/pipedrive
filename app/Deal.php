<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    protected $table = 'deals';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'title',
        'person_name',
        'org_name',
        'status',
        'add_time',
        'update_time'
    ];


    public $timestamps = false;

    public function pipeline()
    {
        return $this->belongsTo('App\Pipeline');
    }


}
